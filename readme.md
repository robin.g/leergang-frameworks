### Opdrachten voor leergang 'bouw je eigen testframework'

Het idee is dat deelnemers zelf leren hoe ze een framework in elkaar zetten. We geven ze opdrachten, maar ze moeten grotendeels zelf uitzoeken hoe ze die moeten voltooien. Hiervoor kunnen ze voornamelijk de documentatie van de te gebruiken talen, tools en libraries gebruiken.

De keuzes voor talen en tools staan vrij. Enkele mogelijke combinaties:
* Java met Maven
* Java met Gradle
* JavaScript/TypeScript met NPM
* JavaScript/TypeScript met Yarn
* C#/.Net met NuGet
* Pyhton met Pip

-------------------------

#### Opdracht 1:
1. Maak een geinformeerde keuze voor een taal en bijbehorende buildtool / package manager.
  * Java met Maven
  * Java met Gradle
  * JavaScript/TypeScript met NPM
  * JavaScript/TypeScript met Yarn
  * C#/.Net met NuGet
  * Pyhton met Pip
2.  Zorg dat je je in ieder geval inleest in drie van de bovengenoemde combinaties en licht volgende standup je keuze toe.

#### Opdracht 2:
1. Start een nieuw framework in je gekozen taal- en toolcombinatie. Je kunt starten via de command line of via IntelliJ. We raden af om een voorbeeldproject te kopieren en hierin te gaan werken. Mocht je een voorbeeldproject gebruiken, doe dat dan ter referentie, en start dus wel je eigen project.
2. Voeg er twee testen aan toe die altijd slagen.
3. Zoek verschillende manieren hoe je je testen kunt draaien. Zorg dat je het in ieder geval kunt vanuit je IDE en via de command line. Zorg dat je tijdens het uitvoeren ook in de console te zien krijgt welke testen er gedraaid worden.

#### Opdracht 3:
1. Kies een online repository met mogelijkheid om pipelines aan te maken. GitLab, Bitbucket enz. Maak ook hier weer een geinformeerde keuze, en licht deze volgende stand-up toe.
2. Maak een nieuwe repository en push je project ernaartoe.
3. Deel je project even met me, zodat ik soms even kan reviewen.

#### Opdracht 4:
1. Maak een pipeline aan. Zorg dat bij iedere push de testen gedraaid worden.
2. Zet je testen ook op een nightly schedule. Iedere nacht moeten dan alle testen gedraaid worden. Check ook voor de zekerheid dat dit gebeurt!

#### Opdracht 5:
1. Voeg een front-end test toe aan je framework:
  1. Ga naar webshop.mobiletestautomation.nl/
  2. Doe een assertion of je op de goede pagina bent aangekomen.
* Tip 1: Het is handig om te beginnen met de webdrivermanager. Die zorgt ervoor dat je niet moeilijk hoeft te gaan doen met drivers downloaden. Hier staat de readme: https://github.com/bonigarcia/webdrivermanager
* Tip 2: In een van de eerste codeblokken in de readme van `webdrivermanager` zie je een mooi voorbeeld van de class ChromeTest. Daar waar staat  `// Your test code here` moet je wat Selenium testcode schrijven waarmee je iets op onze webshop test (webshop.mobiletestautomation.nl). Als je seleniumkennis weer wat is weggezakt, kan ik deze course nog aanraden om even op te frissen: https://testautomationu.applitools.com/selenium-webdriver-tutorial-java/
